import React from "react";
import { Redirect, Route } from "react-router-dom";

function ProtectedRoute({ component }) {
  const Component = component;
  const isAuth = false;

  return isAuth ? <Component /> : <Redirect to="/Login" />;
}

export default ProtectedRoute;
